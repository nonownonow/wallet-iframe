# Iframe Wallet 프로젝트
- iframe을 활용한 지갑 생성 및 코인 전송 프로젝트

## 목차

- [코드 구조](#코드-구조)
    - [핵심 로직](#핵심-로직)
    - [SDK ↔ iframe 통신](#sdk--iframe-통신)
- [시작하기](#시작하기)

## 코드 구조

### 핵심 로직

1. **사용자 상태 관리**
    - **파일**: `UserProvider.tsx`
    - **설명**: 이 파일에서는 React의 `useReducer`를 사용하여 사용자 상태를 관리합니다. `useUserContext`라는 커스텀 훅을 통해 다른 컴포넌트에서 이 상태를 쉽게 접근할 수 있습니다.

2. **지갑 상태 관리**
    - **파일**: `WalletProvider.tsx`
    - **설명**: 지갑 상태 역시 `useReducer`를 사용하여 관리합니다. `useWalletContext`라는 커스텀 훅을 통해 상태를 다룹니다.

3. **프로바이더와 지갑 훅**
    - **파일**: `useProvider.ts`와 `useWallet.ts`
    - **설명**: 이 두 훅에서는 ethers.js 라이브러리를 사용하여 Ethereum 프로바이더와 지갑을 초기화합니다. `useEffect`를 사용하여 의존성 배열에 따라 초기화를 수행합니다.

4. **트랜잭션 처리**
    - **파일**: `Transaction.tsx`
    - **설명**: 이 컴포넌트에서는 트랜잭션의 상태(`PREPARATION`, `SUBMIT`, `PROCESSING`)에 따라 다른 하위 컴포넌트를 렌더링합니다. `Dialog` 컴포넌트를 사용하여 모달을 띄우고 관리합니다.

### SDK ↔ iframe 통신

- **파일**: `App.tsx (iframe)`
- **설명**: 이 파일에서는 `window.addEventListener('message', ...)`를 사용하여 부모 페이지와 메시지를 주고받습니다. `FaceWalletMessage` 타입을 사용하여 특정 액션을 디스패치합니다. 이를 통해 iframe 내부의 상태를 업데이트하거나 특정 작업을 수행합니다.

## 시작하기

1. **패키지 설치 및 빌드**: 프로젝트 루트에서 다음 명령어를 실행하여 패키지를 설치하고 빌드합니다.
    ```bash
    npm install
    npx lerna bootstrap
    npx lerna run build
    ```

2. **서비스 실행**: `iframe`과 `sampledapp` 디렉토리로 이동하여 각각 실행합니다.
    ```bash
    cd packages/iframe
    npm run start

    cd packages/sampledapp
    npm run start
    ```

3. **지갑 템플릿 확인**: 브라우저를 열고 `http://localhost:3000`으로 이동하여 지갑 템플릿을 확인합니다.
