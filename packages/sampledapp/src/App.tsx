import React, { useState } from 'react';
import './App.css';
import { FaceSDK } from '@face/sdk';

function App() {
  const [sdk] = useState(() => FaceSDK.getInstance());
  const [amount, setAmount] = useState('0');
  async function clickCreateWallet() {
    let address;
    try {
      address = await sdk.createWallet();
      console.log('Created Wallet Address:', address);
    } catch (error) {
      console.error('Error in clickCreateWall et:', error);
    }
  }

  async function clickSendTransaction() {
    if (!amount) return;
    const transactionHash = await sdk.sendTransaction(amount);
    console.log(transactionHash);
  }

  return (
    <div className="App">
      <div className="box">
        <button className="btn" onClick={clickCreateWallet}>
          지갑 생성하기
        </button>
      </div>
      <div className="box">
        <div className="label">Amount</div>
        <div className="input-wrapper">
          <input
            type="number"
            className="input-text"
            placeholder="0.00"
            value={amount}
            min={0}
            onChange={(e) => setAmount(e.target.value)}
          />
        </div>
        <button className="btn" onClick={clickSendTransaction} disabled={amount === '0'}>
          트랜잭션 전송하기
        </button>
      </div>
    </div>
  );
}

export default App;
