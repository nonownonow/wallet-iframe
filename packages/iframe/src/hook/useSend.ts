import { createSearchParams, useLocation } from 'react-router-dom';
import { useMemo } from 'react';
import { ethers } from 'ethers';

export const useSend = () => {
  const location = useLocation();
  const search = createSearchParams(location.search);
  const sendStr = search.get('send');
  return useMemo(() => (sendStr ? ethers.utils.parseEther(sendStr) : null), [sendStr]);
};
