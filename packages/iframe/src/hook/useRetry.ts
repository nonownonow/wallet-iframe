import { useEffect, useState } from 'react';

type FetchFunction<T> = () => Promise<T>;
interface RetryOptions {
  maxRetries: number;
  timeout: number;
  exponentialBackoff: boolean;
}
export function useRetry<T>(
  fetchFunction: FetchFunction<T>,
  { maxRetries, timeout, exponentialBackoff = true }: RetryOptions,
  dependency: any[]
): [T | null, Error | null, boolean] {
  const [data, setData] = useState<T | null>(null);
  const [error, setError] = useState<Error | null>(null);
  const [isLoading, setIsLoading] = useState(true);
  const [retryCount, setRetryCount] = useState(0);
  useEffect(() => {
    let timer: ReturnType<typeof setTimeout>;
    let timeoutTimer: ReturnType<typeof setTimeout>;

    const fetchData = async () => {
      setIsLoading(true);
      try {
        const result = await fetchFunction();
        clearTimeout(timeoutTimer);
        setData(result);
        setError(null);
        setRetryCount(0);
      } catch (err) {
        console.error('Error fetching data:', err);
        setError(err);
        setRetryCount((prevCount) => {
          return prevCount + 1;
        });
      } finally {
        setIsLoading(false);
      }
    };

    if (retryCount < maxRetries) {
      if (retryCount === 0) {
        fetchData();
      } else {
        if (exponentialBackoff) {
          timer = setTimeout(() => {
            fetchData();
          }, Math.pow(2, retryCount) * 1000);
        }
      }
      timeoutTimer = setTimeout(() => {
        const error = new Error('타임아웃이 발생하였습니다.');
        console.error(error.message);
        setError(error);
      }, timeout);
    } else if (retryCount === maxRetries) {
      const error = new Error('최대 재시도 횟수에 도달했습니다.');
      console.error(error.message);
      setError(error);
    }

    return () => {
      clearTimeout(timer);
      clearTimeout(timeoutTimer);
    };
  }, [...dependency, retryCount, maxRetries, exponentialBackoff, timeout]);
  return [data, error, isLoading];
}
