import { ethers } from 'ethers';
import { useEffect, useRef, useState } from 'react';

export function useWalletBalance(wallet?: ethers.Wallet, provider?: ethers.providers.Provider) {
  const isMounted = useRef(true);
  const [balance, setBalance] = useState<ethers.BigNumber>();
  useEffect(() => {
    async function fetchBalance() {
      if (!wallet || !provider) return;
      const balanceWei = await provider.getBalance(wallet.address);
      // const balanceEther = ethers.utils.formatEther(balanceWei);
      if (isMounted) {
        setBalance(balanceWei);
      }
    }

    fetchBalance().catch((err) => {
      throw new Error(err.message);
    });

    return () => {
      isMounted.current = false;
    };
  }, [wallet, provider]);

  return balance;
}
