import { useEffect, useState } from 'react';
import { ethers } from 'ethers';

export function useProvider(networkUrl: string) {
  const [provider, setProvider] = useState<ethers.providers.Provider>();
  useEffect(() => {
    if (!networkUrl) throw new Error('NETWORK_URL is not set in the environment variables');
    setProvider(new ethers.providers.JsonRpcProvider(networkUrl));
  }, [networkUrl]);
  return provider;
}
