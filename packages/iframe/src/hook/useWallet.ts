import { ethers } from 'ethers';
import { useRetry } from './useRetry';

export function useWallet(provider?: ethers.providers.Provider) {
  const fetchWallet = async () => {
    if (!provider) return;
    const privateKey = localStorage.getItem('privateKey');
    let fetchedWallet: ethers.Wallet;
    if (!privateKey) {
      fetchedWallet = ethers.Wallet.createRandom().connect(provider);
      localStorage.setItem('privateKey', fetchedWallet?.privateKey);
    } else {
      fetchedWallet = new ethers.Wallet(privateKey).connect(provider);
    }
    return fetchedWallet;
  };

  return useRetry(
    fetchWallet,
    {
      maxRetries: 3,
      timeout: 5000,
      exponentialBackoff: true,
    },
    [provider]
  );
}
