import { useEffect, useRef, useState } from 'react';
import { ethers } from 'ethers';

export const useGasPrice = (provider?: ethers.providers.Provider) => {
  const [gasPrise, setGasPrice] = useState<ethers.BigNumber>();
  const isMounted = useRef(true);
  useEffect(() => {
    async function fetchGasPrice() {
      if (!provider) return;
      const fetchedGasPrice = await provider.getGasPrice();
      if (isMounted.current) {
        setGasPrice(fetchedGasPrice);
      }
    }
    fetchGasPrice().catch((err) => {
      console.error('가스 비용 추정에 실패하였습니다. 기본값 50 gwei 로 셋팅합니다. ');
      if (isMounted.current) {
        const fetchedGasPrice = ethers.utils.parseUnits(process.env.REACT_APP_GAS_PRICE, 'gwei');
        setGasPrice(fetchedGasPrice);
      }
    });
  }, [provider]);
  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, []);
  return gasPrise;
};
