import { useEffect, useRef, useState } from 'react';
import { ethers } from 'ethers';

export const useGasLimit = (
  provider?: ethers.providers.Provider,
  transactionRequest?: ethers.providers.TransactionRequest
) => {
  const [gasLimit, setGasLimit] = useState<ethers.BigNumber>();
  const isMounted = useRef(true);
  useEffect(() => {
    async function fetchGasLimit() {
      if (!provider || !transactionRequest) return;
      const fetchedGasLimit = await provider.estimateGas(transactionRequest);
      if (isMounted.current) {
        setGasLimit(fetchedGasLimit);
      }
    }
    fetchGasLimit().catch((err) => {
      console.error('가스리밋 추정에 실패하였습니다. 기본값 21000 으로 셋팅합니다. ');
      if (isMounted.current) {
        const DEFAULT_GAS_LIMIT = process.env.REACT_APP_GAS_LIMIT as string;
        setGasLimit(ethers.BigNumber.from(DEFAULT_GAS_LIMIT));
      }
    });
  }, [provider, transactionRequest]);
  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, []);
  return gasLimit;
};
