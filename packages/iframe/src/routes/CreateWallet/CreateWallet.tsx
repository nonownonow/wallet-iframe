import React from 'react';
import { Dialog } from 'src/componet/Dialog/Dialog';
import 'src/routes/CreateWallet/CreateWallet.css';
import { useTranslation } from 'react-i18next';
import { Noop } from 'src/util';
import { Button } from 'src/componet/Button/Button';
import { FaceWalletMessage } from '@face/sdk/lib/types';
import { useUserContext } from 'src/store/user/UserProvider';
import { Helmet } from 'react-helmet';

export default function CreateWallet() {
  const { t } = useTranslation('wallet.create');
  const { state: userState } = useUserContext();
  const { wallet, messageId } = userState;
  function handleClose() {
    if (!wallet) return;
    const message: FaceWalletMessage = { id: messageId, result: wallet.address };
    window.parent.postMessage(message, '*');
  }
  return (
    <Dialog open Header={Noop}>
      <Helmet>
        <title>{t('title')}</title>
      </Helmet>
      <article data-fx-sectioning data-wallet>
        <h2 data-fx-heading>{t('h')}</h2>
        <section data-fx-sectioning data-success>
          <h3 data-fx-heading>{t('sec_main.h')}</h3>
          <p data-ready>{t('sec_main.p')}</p>
          <p data-address>{wallet?.address}</p>
        </section>
        <Button onClick={handleClose} $data={t('button')} />
      </article>
    </Dialog>
  );
}
