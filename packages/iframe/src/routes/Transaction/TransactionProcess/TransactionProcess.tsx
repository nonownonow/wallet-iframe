import React, { ComponentPropsWithoutRef, forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { OpenInNew } from '@mui/icons-material';
import { html } from 'src/util';
import { TransactionCard } from '../component/TransactionCard/TransactionCard';
import { AnchorButton } from 'src/componet/AnchorButton/AnchorButton';
import './TransactionProcess.css';
import { useWalletContext } from 'src/store/wallet/WalletProvider';
import { Helmet } from 'react-helmet';

export type ProcessProps = ComponentPropsWithoutRef<'article'>;
export const TransactionProcess = forwardRef<HTMLElement, ProcessProps>((props, ref) => {
  const { t } = useTranslation('wallet.transaction.process');
  const { state: walletState } = useWalletContext();
  const { hash } = walletState;
  const ETHER_SCAN = `${process.env.REACT_APP_TRANSACTION_URL}${hash}`;
  return (
    <article ref={ref} data-transaction data-processing>
      <Helmet>
        <title>{t('title')}</title>
      </Helmet>
      <h2 data-fx-heading>{t('h')}</h2>
      <section data-main>
        <h3>{t('sec_main.h')}</h3>
        <p {...html(t('sec_main.p', { defaultValue: '' }))}></p>
      </section>
      <TransactionCard $data={walletState} $keys={['status', 'to', 'amount', 'fee', 'total']} />
      <AnchorButton href={ETHER_SCAN} target={'_blank'}>
        <span {...html(t('button'))} /> <OpenInNew data-icon={'open-in-new'} />
      </AnchorButton>
    </article>
  );
});
export default TransactionProcess;
