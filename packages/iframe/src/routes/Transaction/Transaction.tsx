import React, { lazy, useEffect } from 'react';
import './Transaction.css';
import { Dialog } from 'src/componet/Dialog/Dialog';

import { useWalletContext } from 'src/store/wallet/WalletProvider';
import { FaceWalletMessage } from '@face/sdk/lib/types';
import { useUserContext } from 'src/store/user/UserProvider';
import { OpenseaSymbol } from 'src/componet/Icon/Icon';
import { Close } from '@mui/icons-material';

const TransactionSubmit = lazy(
  () => import('src/routes/Transaction/TransactionSubmit/TransactionSubmit')
);
const TransactionProcess = lazy(
  () => import('src/routes/Transaction/TransactionProcess/TransactionProcess')
);
const TransactionComplete = lazy(
  () => import('src/routes/Transaction/TransactionComplete/TransactionComplete')
);

const DefaultHeader = () => {
  const { state: walletState, dispatch: walletDispatch } = useWalletContext();
  const { state: userState } = useUserContext();
  const { wallet } = userState;
  const { status } = walletState;
  function handleClose() {
    const { messageId } = userState;
    const message: FaceWalletMessage = { id: messageId, result: wallet?.address };
    window.parent.postMessage(message, '*');
    walletDispatch({ type: 'SET_STATUS', payload: 'PREPARATION' });
  }
  return (
    <header>
      <OpenseaSymbol data-icon={'logo-opensea'} />
      {['PREPARATION', 'COMPLETE'].includes(status) ? (
        <Close data-icon={'close'} onClick={handleClose} />
      ) : null}
    </header>
  );
};
export default function Transaction() {
  const { state: walletState, dispatch: walletDispatch } = useWalletContext();
  const { status } = walletState;
  useEffect(() => {
    walletDispatch({ type: 'SET_STATUS', payload: 'PREPARATION' });
  }, [walletDispatch]);
  return (
    <Dialog open Header={DefaultHeader}>
      {status === 'PREPARATION' || status === 'SUBMIT' ? (
        <TransactionSubmit />
      ) : status === 'PROCESSING' ? (
        <TransactionProcess />
      ) : (
        <TransactionComplete />
      )}
    </Dialog>
  );
}
