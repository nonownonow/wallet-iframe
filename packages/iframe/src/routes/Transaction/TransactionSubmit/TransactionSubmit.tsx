import React, { ComponentProps, forwardRef, useEffect, useMemo, useState } from 'react';
import { html } from 'src/util';
import { TransactionCard } from 'src/routes/Transaction/component/TransactionCard/TransactionCard';
import { useTranslation } from 'react-i18next';
import './TransactionSubmit.css';
import { parseWithUnit } from 'src/util/format';
import { Button } from 'src/componet/Button/Button';
import { useWalletContext } from 'src/store/wallet/WalletProvider';
import { useUserContext } from 'src/store/user/UserProvider';
import { ethers } from 'ethers';
import { Spinner } from 'src/componet/Icon/Icon';
import { useLocation } from 'react-router-dom';
import { useWalletBalance } from 'src/hook/useWalletBalance';
import { useGasPrice } from 'src/hook/useGasPrice';
import { useGasLimit } from 'src/hook/useGasLimit';
import { useSend } from 'src/hook/useSend';
import { Helmet } from 'react-helmet';

export type TransactionSubmitProps = ComponentProps<'article'>;

export const TransactionSubmit = forwardRef<HTMLElement, TransactionSubmitProps>((props, ref) => {
  const { t } = useTranslation('wallet.transaction.submit');
  const [disabled, setDisabled] = useState(true);
  const [isInSufficientError, setIsInSufficientError] = useState(false);
  const [loading, setLoading] = useState(false);
  const { state: userState } = useUserContext();
  const { state: walletState, dispatch: walletDispatch } = useWalletContext();
  const { provider, wallet } = userState;
  const { to, amount, insufficient } = walletState;
  const location = useLocation();
  const send = useSend();
  const balanceWei = useWalletBalance(wallet, provider);
  const gasPrice = useGasPrice(provider);
  const transactionRequest: ethers.providers.TransactionRequest = useMemo(() => {
    if (!send) return {};
    return {
      to: walletState.to,
      value: send,
    };
  }, [send, walletState.to]);
  const gasLimit = useGasLimit(provider, transactionRequest);
  useEffect(() => {
    if (send) {
      walletDispatch({ type: 'SET_AMOUNT', payload: send });
    }
    if (gasPrice && gasLimit && send && balanceWei) {
      const fee = gasPrice.mul(gasLimit);
      const total = send.add(fee);

      walletDispatch({ type: 'SET_TOTAL', payload: total });
      walletDispatch({ type: 'SET_FEE', payload: fee });
      walletDispatch({ type: 'SET_AVAILABLE', payload: balanceWei });
      walletDispatch({ type: 'SET_GAS_PRICE', payload: gasPrice });
      walletDispatch({ type: 'SET_GAS_LIMIT', payload: gasLimit });
      if (balanceWei.lt(total)) {
        const insufficientFundInWei = send.add(fee).sub(balanceWei);
        walletDispatch({ type: 'SET_INSUFFICIENT', payload: insufficientFundInWei });
      } else {
        walletDispatch({ type: 'SET_INSUFFICIENT', payload: undefined });
        setIsInSufficientError(false);
      }
    }
  }, [wallet, provider, location.search, balanceWei, walletDispatch, gasPrice, gasLimit, send]);
  const handleTransactionSubmit = async () => {
    setLoading(true);
    setDisabled(true);
    walletDispatch({ type: 'SET_STATUS', payload: 'SUBMIT' });
    try {
      const nonce = await provider?.getTransactionCount(wallet?.address as string);
      const network = await provider?.getNetwork();
      const transaction: ethers.providers.TransactionRequest = {
        to: to,
        value: amount,
        gasPrice: gasPrice,
        gasLimit: gasLimit,
        chainId: network?.chainId,
        nonce: nonce,
      };
      const tx = await wallet?.sendTransaction(transaction);
      walletDispatch({ type: 'SET_STATUS', payload: 'PROCESSING' });
      walletDispatch({ type: 'SET_HASH', payload: tx?.hash });
      await tx?.wait();
      walletDispatch({ type: 'SET_STATUS', payload: 'COMPLETE' });
    } catch (err) {
      console.error('트랜잭션 전송 시 에러가 발생하였습니다.', err);
    } finally {
      setDisabled(false);
      setLoading(false);
    }
  };

  const insufficientContents = useMemo(
    () => parseWithUnit(insufficient, { unit: 'ETH' }),
    [insufficient]
  );

  useEffect(() => {
    if (insufficient) {
      setDisabled(true);
      setIsInSufficientError(true);
    }
    const isPrepareTransaction = to && amount && gasPrice && gasLimit && provider && wallet;
    if (isPrepareTransaction && !insufficient) {
      setDisabled(false);
    }
  }, [amount, gasLimit, gasPrice, insufficient, provider, to, wallet]);
  return (
    <article data-fx-sectioning data-transaction data-submit ref={ref}>
      <Helmet>
        <title>{t('title')}</title>
      </Helmet>
      <h2 data-fx-heading>{t('h')}</h2>
      <section data-main>
        <h3 data-fx-heading>{t('sec_main.h')}</h3>
        <p data-send {...html(parseWithUnit(walletState.amount))} />
      </section>
      <TransactionCard data-available $data={walletState} $keys={['available']} />
      <TransactionCard $data={walletState} $keys={['to', 'amount', 'fee']} />
      {isInSufficientError && (
        <p data-error={'INSUFFICIENT'} {...html(`${t('p_error')} ${insufficientContents}`)} />
      )}
      <Button onClick={handleTransactionSubmit} disabled={disabled}>
        {loading ? <Spinner /> : null}
        {t('button')}
      </Button>
    </article>
  );
});
export default TransactionSubmit;
