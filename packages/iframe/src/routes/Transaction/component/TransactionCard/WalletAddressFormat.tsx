import { useTranslation } from 'react-i18next';
import React, { useEffect, useRef, useState } from 'react';
import { gsap } from 'gsap';
import { Tooltip } from 'src/componet/Tooltip/Tooltip';
import { shortenAddress } from 'src/util';

export interface WalletAddressFormatProps {
  value: string;
}

export const WalletAddressFormat = ({ value }: WalletAddressFormatProps) => {
  const { t } = useTranslation('wallet.transaction.process');
  const [content, setContent] = useState(value);
  const tooltipRef = useRef<HTMLSpanElement>(null);
  const tooltipTimeline = useRef<gsap.core.Timeline>();
  async function handleClick() {
    if (tooltipRef.current === null) return;
    else {
      tooltipTimeline.current = gsap.timeline().to(tooltipRef.current, {
        duration: 0,
        backgroundColor: 'rgb(220, 225, 232)',
      });
    }

    setContent(t('span_copy'));
    try {
      await navigator.clipboard.writeText(value);
    } catch (err) {
      console.error('Failed to copy text: ', err);
    }
  }
  function handleLeave() {
    if (tooltipTimeline.current)
      tooltipTimeline.current.to(tooltipRef.current, {
        duration: 0.5,
        backgroundColor: 'transparent',
      });
  }
  function handleEnter() {
    setContent(value);
  }

  useEffect(() => {
    return () => {
      if (tooltipTimeline.current) {
        tooltipTimeline.current.kill();
      }
    };
  }, []);
  return (
    <Tooltip
      ref={tooltipRef}
      content={content}
      onClick={handleClick}
      onMouseLeave={handleLeave}
      onMouseEnter={handleEnter}>
      {shortenAddress(value)}
    </Tooltip>
  );
};
