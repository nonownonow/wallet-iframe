import React, { forwardRef } from 'react';
import { Card, CardProps } from 'src/componet/Card/Card';
import { useTranslation } from 'react-i18next';
import './TransactionCard.css';
import { TransactionState } from 'types/wallet';
import { Spinner } from 'src/componet/Icon/Icon';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { lowerCase, unary, upperFirst } from 'lodash';
import { WalletAddressFormat } from 'src/routes/Transaction/component/TransactionCard/WalletAddressFormat';
import { parseWithUnit } from 'src/util/format';
import { Wallet } from 'src/store/wallet/wallet.state';

export type TransactionCardProps = Omit<CardProps, '$data'> & {
  $data: Wallet;
};
export const TransactionCard = forwardRef<HTMLDListElement, TransactionCardProps>((props, ref) => {
  const { t } = useTranslation();
  const { $data, $keys, ...transactionCardProps } = props;

  return (
    <Card
      ref={ref}
      data-transaction
      {...transactionCardProps}
      $data={$data}
      $keys={$keys}
      $keyFormats={t('wallet')}
      $valueFormats={{
        send: unary(parseWithUnit),
        available: unary(parseWithUnit),
        status: (value: TransactionState) => (
          <>
            {value === 'PROCESSING' && <Spinner />}
            {value === 'COMPLETE' && <CheckCircleIcon data-icon={'check-circle-icon'} />}
            {upperFirst(lowerCase(value))}
          </>
        ),
        to: (value: string) => <WalletAddressFormat value={value} />,
        amount: unary(parseWithUnit),
        fee: unary(parseWithUnit),
        total: unary(parseWithUnit),
      }}
    />
  );
});
