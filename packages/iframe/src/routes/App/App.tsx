import React, { useEffect, useState } from 'react';
import { createSearchParams, Outlet, useLocation, useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import './App.css';
import { useUserContext } from 'src/store/user/UserProvider';
import { FaceWalletMessage } from '@face/sdk/lib/types';
import { searchToObject } from 'src/util';
import { useProvider } from 'src/hook/useProvider';
import { useWallet as useWallet2 } from 'src/hook/useWallet';

function App() {
  const { t } = useTranslation();
  const { state, dispatch } = useUserContext();
  const navigate = useNavigate();
  const location = useLocation();
  const [address, setAddress] = useState<string>();
  const networkUrl = process.env.REACT_APP_NETWORK_URL;
  const provider = useProvider(networkUrl);
  const [wallet] = useWallet2(provider);
  useEffect(() => {
    window.addEventListener('message', async (event: MessageEvent<FaceWalletMessage>) => {
      if (event.data.serviceId === 'face-wallet') {
        dispatch({ type: 'SET_MESSAGE_ID', payload: event.data.id });
        if (event.data.action) {
          dispatch(event.data.action);
        }
      }
    });
  }, [dispatch]);
  useEffect(() => {
    dispatch({
      type: 'SET_CURRENT_ROUTE',
      payload: { path: location.pathname as any, query: searchToObject(location.search) },
    });
  }, [dispatch, location.pathname, location.search]);
  useEffect(() => {
    if (state.currentRoute.path !== null) {
      const searchParams = createSearchParams(state.currentRoute.query);
      navigate(`${state.currentRoute.path}?${searchParams.toString()}`);
    }
  }, [dispatch, state.currentRoute.path, navigate, location.pathname, state.currentRoute.query]);

  useEffect(() => {
    if (wallet) {
      setAddress(wallet.address);
      dispatch({ type: 'SET_WALLET', payload: wallet });
      dispatch({ type: 'SET_PRIVATE_KEY', payload: wallet.privateKey });
    }
  }, [dispatch, wallet]);
  useEffect(() => {
    if (!provider) return;
    dispatch({ type: 'SET_PROVIDER', payload: provider });
  }, [dispatch, provider]);
  return (
    <article data-fx-sectioning data-app>
      <h1 data-fx-heading>{t('h')}</h1>
      <Outlet />
    </article>
  );
}

export default App;
