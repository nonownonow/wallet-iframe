import React, { lazy } from 'react';
import ReactDOM from 'react-dom/client';
import App from './routes/App/App';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import 'destyle.css';
import './i18n';
import './styles/index.css';
import { UserProvider } from 'src/store/user/UserProvider';
import { WalletProvider } from 'src/store/wallet/WalletProvider';

const CreateWallet = lazy(() => import('src/routes/CreateWallet/CreateWallet'));
const Transaction = lazy(() => import('src/routes/Transaction/Transaction'));
const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);
const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      { path: 'create-wallet', element: <CreateWallet /> },
      {
        path: 'transaction',
        element: (
          <WalletProvider>
            <Transaction />
          </WalletProvider>
        ),
      },
    ],
  },
  {
    path: '*',
    element: (
      <article>
        <h1>페이지를 찾을 수 없습니다.</h1>
        <p>해당하는 웹페이지가 없습니다. 주소를 다시 확인하여 주세요.</p>
      </article>
    ),
  },
]);
root.render(
  <UserProvider>
    <RouterProvider router={router} />
  </UserProvider>
);
