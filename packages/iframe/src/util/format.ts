import { ethers } from 'ethers';

const MAXIMUM_FRACTION_DIGITS = 20;
export const formatter = new Intl.NumberFormat('en-US', {
  maximumFractionDigits: MAXIMUM_FRACTION_DIGITS,
});

type withUnitOpt = {
  unit?: 'MATIC' | 'ETH';
  precision?: number;
};

export const parseWithUnit = (value?: ethers.BigNumber, option?: withUnitOpt) => {
  const { unit = 'MATIC', precision = 6 } = option || {};
  if (!value) return '';
  const number = parseFloat(ethers.utils.formatEther(value));
  const multiplier = Math.pow(10, precision);
  const newNumber = Math.round(number * multiplier) / multiplier;
  return `<span data-amount>${newNumber}</span><span data-unit> ${unit}</span>`;
};
