import { ForwardedRef, MutableRefObject, ReactNode } from 'react';

export function shortenAddress(address: string, charsToShow = 4): string {
  if (!address) return '';
  if (address.length < charsToShow * 2 + 2) {
    return address;
  }
  const prefix = address.slice(0, charsToShow);
  const suffix = address.slice(-charsToShow);
  return `${prefix}...${suffix}`;
}

export function html(html: ReactNode) {
  let result;
  if (typeof html === 'string') {
    // if (["string", "number", "boolean"].includes(typeof html)) {
    result = {
      dangerouslySetInnerHTML: {
        __html: html,
      },
    };
  } else {
    result = {
      children: html,
    };
  }
  return result;
}

export function setMultipleRef<T>(refs: (ForwardedRef<T> | MutableRefObject<T>)[]) {
  return function (instance: T) {
    refs.forEach((ref) => {
      if (typeof ref === 'function') {
        ref(instance);
      } else if (ref !== null) {
        ref.current = instance;
      }
    });
  };
}
export function isPlainObject(obj: Record<string, any>) {
  return typeof obj === 'object' && !Array.isArray(obj) && obj !== null;
}

export const Noop = () => null;

export function searchToObject(search: string) {
  const params = new URLSearchParams(search);
  const queryObject: Record<string, string> = {};
  for (const [k, v] of params.entries()) {
    queryObject[k] = v;
  }
  return queryObject;
}
