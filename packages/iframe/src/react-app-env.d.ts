/// <reference types="react-scripts" />
declare namespace NodeJS {
  interface ProcessEnv {
    REACT_APP_NETWORK_URL: string;
    REACT_APP_GAS_PRICE: string;
    REACT_APP_TRANSACTION_URL: string;
    REACT_APP_FETCH_RETRY: string;
    REACT_APP_RETRY_TIMEOUT: string;
  }
}

declare interface Window {
  __REDUX_DEVTOOLS_EXTENSION__?: any;
}
