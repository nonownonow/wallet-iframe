import { identity } from '@fxts/core';
import { ComponentPropsWithoutRef, forwardRef, ReactNode, useMemo } from 'react';
import { html, isPlainObject } from '../../util';
import './Card.css';

export type CardProps = ComponentPropsWithoutRef<'dl'> & {
  /**
   * 객체 데이터
   */
  $data: Record<string, any>;
  /**
   * 객체의 엔트리 표시 순서 및 표시 여부를 제어하는 키 배열
   */
  $keys?: string[];
  /**
   * 키에 포멧을 적용하는 함수
   * */
  $keyFormat?: (key: string, index: number) => ReactNode;
  /**
   * 키에 개별적으로 포멧을 적용하는 함수 또는 값
   * */
  $keyFormats?: Record<string, CardProps['$keyFormat'] | ReactNode>;
  /**
   * 값에 포멧을 적용하는 함수
   * */
  $valueFormat?: (value: any, key: string, index: number) => ReactNode;
  /**
   * 값에 개별적으로 포멧을 적용하는 함수 또는 값
   * */
  $valueFormats?: Record<string, CardProps['$valueFormat'] | ReactNode>;
};

export const Card = forwardRef<HTMLDListElement, CardProps>(function (props, ref) {
  const {
    $data,
    $keys = Object.keys($data),
    $keyFormat = identity,
    $keyFormats = {} as Record<string, any>,
    $valueFormat = identity,
    $valueFormats = {} as Record<string, any>,
    ...cardProps
  } = props;
  const entries = useMemo(() => {
    return $keys.map((key, index) => {
      const value = $data[key];
      const keyLabel =
        key in $keyFormats
          ? typeof $keyFormats[key] === 'string'
            ? $keyFormats[key]
            : isPlainObject($keyFormats[key])
            ? key
            : $keyFormats[key](key, index)
          : $keyFormat(key, index);
      const valueLabel =
        key in $valueFormats
          ? typeof $valueFormats[key] === 'string'
            ? $valueFormats[key]
            : $valueFormats[key]($data[key], key, index)
          : $valueFormat($data[key], key, index);
      return (
        <div key={key} data-fx-entry data-entry={key}>
          <dt data-key={key} {...html(keyLabel)} />
          <dd
            data-loading={value === undefined ? true : undefined}
            data-value={value ?? ''}
            {...html(valueLabel)}
          />
        </div>
      );
    });
  }, [$data, $keys, $keyFormat, $keyFormats, $valueFormat, $valueFormats]);
  return (
    <dl data-fx-card {...cardProps} ref={ref}>
      {entries}
    </dl>
  );
});
