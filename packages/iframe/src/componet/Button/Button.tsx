import { ComponentProps, forwardRef, ReactNode } from 'react';
import { html } from 'src/util';
import './Button.css';

export type ButtonProps = ComponentProps<'button'> & {
  $data?: string;
  children?: ReactNode;
};

export const Button = forwardRef<HTMLButtonElement, ButtonProps>((props, ref) => {
  const { $data, children, ...buttonProps } = props;
  return (
    <button data-fx-button data-primary ref={ref} {...buttonProps} {...html(children || $data)} />
  );
});
