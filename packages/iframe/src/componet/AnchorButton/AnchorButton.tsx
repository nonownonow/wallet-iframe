import { ComponentProps, forwardRef, ReactNode } from 'react';
import { html } from 'src/util';
import 'src/componet/AnchorButton/AnchorButton.css';

export type ButtonProps = ComponentProps<'a'> & {
  $data?: string;
  children?: ReactNode;
};

export const AnchorButton = forwardRef<HTMLAnchorElement, ButtonProps>((props, ref) => {
  const { $data, children, ...aProps } = props;
  return <a data-fx-button data-fx-a ref={ref} {...aProps} {...html(children || $data)} />;
});
