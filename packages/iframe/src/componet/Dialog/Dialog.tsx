import type { ComponentPropsWithoutRef, FC } from 'react';
import React, { forwardRef } from 'react';
import './Dialog.css';
import { Logo } from '../Icon/Icon';
import { Noop } from 'src/util';

export type DialogProps = ComponentPropsWithoutRef<'dialog'> & {
  Header?: FC;
};

export const Dialog = forwardRef<HTMLDialogElement, DialogProps>(function Dialog(props, ref) {
  const { children, Header = Noop, ...restProps } = props;
  return (
    <dialog data-fx-dialog {...restProps} ref={ref}>
      <div data-contents>
        <Header />
        <main>{children}</main>
        <footer>
          <div data-copyright>
            <div>Powered by</div>
            <Logo data-icon={'logo'} />
          </div>
        </footer>
      </div>
    </dialog>
  );
});
/*
*     <dialog open data-fx-dialog>
      <div data-contents>
        <header>
          <OpenseaSymbol data-icon={'logo-opensea'} width={'4.8rem'} height={'4.8rem'} />
          <CloseIcon data-icon={'close'} width={'2.4rem'} height={'2.4rem'} />
        </header>
        <main>
          <article data-transaction data-processing>
            <h2 data-fx-heading>{t('h')}</h2>
            <dl data-fx-card data-transaction>
              <div data-entry={'send'}>
                <dt>{t('card_transaction.send')}</dt>
                <dd>
                  {card_transaction.send}
                  <span data-currency-unit> {currencyUnit}</span>
                </dd>
              </div>
              <div data-entry={'available'}>
                <dt>{t('card_transaction.available')}</dt>
                <dd>
                  <span data-amount>{formatter.format(card_transaction.available)}</span>{' '}
                  {currencyUnit}
                </dd>
              </div>
              <div className={'transaction_info'}>
                <dd>
                  <dl data-fx-card data-transaction-info>
                    <div data-entry={'to'}>
                      <dt>{t('card_transaction.transaction_info.to')}</dt>
                      <dd title={card_transaction.transaction_info.to}>
                        {shortenAddress(card_transaction.transaction_info.to)}
                      </dd>
                    </div>
                    <div data-entry={'amount'}>
                      <dt>{t('card_transaction.transaction_info.amount')}</dt>
                      <dd>
                        {card_transaction.transaction_info.amount} {currencyUnit}
                      </dd>
                    </div>
                    <div data-entry={'fee'}>
                      <dt>{t('card_transaction.transaction_info.fee')}</dt>
                      <dd>
                        {card_transaction.transaction_info.fee} {currencyUnit}
                      </dd>
                    </div>
                  </dl>
                </dd>
              </div>
            </dl>
            <p data-error>{error}</p>
          </article>
        </main>
        <footer>
          <button data-fx-button data-primary disabled={!!error}>
            {loading && <Loading data-icon={'spinner'} />}
            {t('button_confirm')}
          </button>
          <div data-copyright>
            <div>Powered by</div>
            <Logo data-icon={'logo'} />
          </div>
        </footer>
      </div>
    </dialog>
* */
