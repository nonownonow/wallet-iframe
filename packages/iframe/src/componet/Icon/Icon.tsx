import 'src/componet/Icon/Icon.css';
import { ComponentProps, forwardRef } from 'react';
import { ReactComponent as _Loading } from './img/ring-resize.svg';

export { ReactComponent as OpenseaSymbol } from './img/opensea-symbol.svg';
export { ReactComponent as Logo } from './img/logo.svg';

export const Spinner = forwardRef<SVGSVGElement, ComponentProps<'svg'>>((props, ref) => (
  <_Loading data-icon={'spinner'} {...props} ref={ref} />
));
