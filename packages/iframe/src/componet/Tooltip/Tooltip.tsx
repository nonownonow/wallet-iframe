import { ComponentPropsWithRef, forwardRef, MouseEvent, useEffect, useRef } from 'react';
import './Tooltip.css';
import { gsap } from 'gsap';
import { identity } from '@fxts/core';

export type TooltipProps = ComponentPropsWithRef<'span'> & {
  content: string;
};
export const Tooltip = forwardRef<HTMLSpanElement, TooltipProps>((props, ref) => {
  const {
    children,
    content,
    onClick = identity,
    onMouseEnter = identity,
    onMouseLeave = identity,
    ...tooltipProps
  } = props;
  const tooltipRef = useRef<HTMLSpanElement>(null);
  const animationTimeline = useRef<gsap.core.Timeline | null>();

  function playAnimation() {
    if (!tooltipRef.current) return;
    if (animationTimeline.current) {
      animationTimeline.current.restart();
    } else {
      animationTimeline.current = gsap.timeline().to(tooltipRef.current, {
        visibility: 'visible',
        duration: 0.3,
        ease: 'none',
        autoAlpha: 0.7,
      });
    }
  }
  const handleClick = (e: MouseEvent<HTMLSpanElement>) => {
    onClick(e);
    playAnimation();
  };
  const handleMouseEnter = (e: MouseEvent<HTMLSpanElement>) => {
    onMouseEnter(e);
    playAnimation();
  };
  const handleMouseLeave = (e: MouseEvent<HTMLSpanElement>) => {
    onMouseLeave(e);
    if (animationTimeline.current) {
      animationTimeline.current.reverse();
    }
  };
  useEffect(() => {
    return () => {
      if (animationTimeline.current) {
        animationTimeline.current.kill();
      }
    };
  }, []);
  return (
    <span
      data-fx-tooltip
      ref={ref}
      {...tooltipProps}
      onClick={handleClick}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}>
      <>
        {children}
        <span data-content ref={tooltipRef}>
          {content}
        </span>
      </>
    </span>
  );
});
