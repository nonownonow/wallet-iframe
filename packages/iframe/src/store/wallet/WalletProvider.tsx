import { createContext, Dispatch, FC, ReactNode, useContext, useReducer } from 'react';
import { initialWalletState, Wallet } from 'src/store/wallet/wallet.state';
import { WalletAction, walletReducer } from 'src/store/wallet/wallet.reducer';

type WalletContext = {
  state: Wallet;
  dispatch: Dispatch<WalletAction>;
};
export const walletContext = createContext<WalletContext | undefined>(undefined);

export const WalletProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [state, dispatch] = useReducer(walletReducer, initialWalletState);
  return <walletContext.Provider value={{ state, dispatch }}>{children}</walletContext.Provider>;
};

export const useWalletContext = () => {
  const context = useContext(walletContext);
  if (!context) {
    throw new Error('useWalletContext 는 WalletProvider 안에서 사용할 수 있습니다.');
  }
  return context;
};
