import { ethers } from 'ethers';

export type TransactionState = 'PREPARATION' | 'SUBMIT' | 'PROCESSING' | 'COMPLETE';

export interface Wallet {
  status: TransactionState;
  available: ethers.BigNumber | undefined;
  send: ethers.BigNumber | undefined;
  to: string;
  amount: ethers.BigNumber | undefined;
  fee: ethers.BigNumber | undefined;
  total?: ethers.BigNumber | undefined;
  insufficient?: ethers.BigNumber | undefined;
  gasPrice?: ethers.BigNumber | undefined;
  gasLimit?: ethers.BigNumber | undefined;
  hash?: string | undefined;
}

export const initialWalletState: Wallet = {
  status: 'PREPARATION',
  available: undefined,
  send: undefined,
  amount: undefined,
  fee: undefined,
  total: undefined,
  insufficient: undefined,
  gasPrice: undefined,
  gasLimit: undefined,
  to: '0x27E982b4Fde66764bA27A681605C2912A0A50422',
  hash: undefined,
};
