import { initialWalletState, TransactionState, Wallet } from 'src/store/wallet/wallet.state';
import { ethers } from 'ethers';

export type WalletAction =
  | { type: 'SET_STATUS'; payload: TransactionState }
  | { type: 'RESET_WALLET' }
  | { type: 'SET_AVAILABLE'; payload: ethers.BigNumber | undefined }
  | { type: 'SET_INSUFFICIENT'; payload: ethers.BigNumber | undefined }
  | { type: 'SET_TO'; payload: string }
  | { type: 'SET_SEND'; payload: ethers.BigNumber | undefined }
  | { type: 'SET_AMOUNT'; payload: ethers.BigNumber | undefined }
  | { type: 'SET_FEE'; payload: ethers.BigNumber | undefined }
  | { type: 'SET_TOTAL'; payload: ethers.BigNumber | undefined }
  | { type: 'SET_HASH'; payload: string | undefined }
  | { type: 'SET_GAS_PRICE'; payload: ethers.BigNumber | undefined }
  | { type: 'SET_GAS_LIMIT'; payload: ethers.BigNumber | undefined };

export function walletReducer(state: Wallet, action: WalletAction) {
  if (process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION__) {
    window.__REDUX_DEVTOOLS_EXTENSION__.send(action, state);
  }
  switch (action.type) {
    case 'SET_STATUS':
      return { ...state, status: action.payload };
    case 'SET_AVAILABLE':
      return { ...state, available: action.payload };
    case 'SET_INSUFFICIENT':
      return { ...state, insufficient: action.payload };
    case 'SET_TO':
      return { ...state, to: action.payload };
    case 'SET_SEND':
      return { ...state, send: action.payload };
    case 'SET_AMOUNT':
      return { ...state, amount: action.payload };
    case 'SET_FEE':
      return { ...state, fee: action.payload };
    case 'SET_TOTAL':
      return { ...state, total: action.payload };
    case 'SET_GAS_PRICE':
      return { ...state, gasPrice: action.payload };
    case 'SET_GAS_LIMIT':
      return { ...state, gasLimit: action.payload };
    case 'SET_HASH':
      return { ...state, hash: action.payload };
    case 'RESET_WALLET':
      return initialWalletState;
  }
}
