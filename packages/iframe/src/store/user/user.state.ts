import { ethers } from 'ethers';

export interface TransactionQuery {
  send: string;
  [key: string]: string;
}
export type currentRoute =
  | { path: null; query?: Record<string, any> }
  | { path: '/'; query?: Record<string, any> }
  | { path: '/create-wallet'; query?: Record<string, any> }
  | { path: '/transaction'; query: TransactionQuery };

export interface User {
  privateKey: string;
  wallet: ethers.Wallet | undefined;
  provider: ethers.providers.Provider | undefined;
  currentRoute: currentRoute;
  messageId: string | undefined;
}
export const initialUserState: User = {
  privateKey: '',
  wallet: undefined,
  provider: undefined,
  currentRoute: {
    path: null,
  },
  messageId: '',
};
