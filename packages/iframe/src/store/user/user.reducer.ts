import { ethers } from 'ethers';
import { currentRoute, initialUserState, TransactionQuery, User } from 'src/store/user/user.state';

export type FaceWalletAction =
  | { type: 'CREATE_WALLET' }
  | {
      type: 'TRANSACTION';
      payload: TransactionQuery;
    };

export type UserAction =
  | { type: 'SET_PRIVATE_KEY'; payload: string }
  | { type: 'SET_WALLET'; payload: ethers.Wallet }
  | { type: 'SET_PROVIDER'; payload: ethers.providers.Provider }
  | { type: 'SET_CURRENT_ROUTE'; payload: currentRoute }
  | { type: 'SET_MESSAGE_ID'; payload: string | undefined }
  | { type: 'RESET_USER' }
  | FaceWalletAction;

export function userReducer(state: User, action: UserAction): User {
  if (process.env.NODE_ENV === 'development' && window.__REDUX_DEVTOOLS_EXTENSION__) {
    window.__REDUX_DEVTOOLS_EXTENSION__.send(action, state);
  }
  switch (action.type) {
    case 'SET_PRIVATE_KEY':
      return { ...state, privateKey: action.payload };
    case 'SET_PROVIDER':
      return { ...state, provider: action.payload };
    case 'SET_WALLET':
      return { ...state, wallet: action.payload };
    case 'SET_CURRENT_ROUTE':
      return { ...state, currentRoute: action.payload };
    case 'SET_MESSAGE_ID':
      return { ...state, messageId: action.payload };
    case 'CREATE_WALLET':
      return { ...state, currentRoute: { path: '/create-wallet' } };
    case 'TRANSACTION':
      return { ...state, currentRoute: { path: '/transaction', query: action.payload } };
    case 'RESET_USER':
      return initialUserState;
    default:
      return state;
  }
}
