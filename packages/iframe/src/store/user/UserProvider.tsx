import { createContext, Dispatch, FC, ReactNode, useContext, useReducer } from 'react';
import { UserAction, userReducer } from 'src/store/user/user.reducer';
import { initialUserState, User } from 'src/store/user/user.state';

const UserContext = createContext<{ state: User; dispatch: Dispatch<UserAction> } | undefined>(
  undefined
);

export const UserProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [state, dispatch] = useReducer(userReducer, initialUserState);
  return <UserContext.Provider value={{ state, dispatch }}>{children}</UserContext.Provider>;
};

export function useUserContext() {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error('useUserContext 는 UserProvider 안에서 실행 되어야 합니다.');
  }
  return context;
}
