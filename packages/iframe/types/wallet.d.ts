// export interface Wallet {
//   available: number;
//   send: number;
// }
import { ethers } from 'ethers';

export type TransactionState = 'SUBMIT' | 'PROCESSING' | 'COMPLETE';
export interface Wallet {
  status: TransactionState;
  available: ethers.BigNumber | undefined;
  send: number;
  to: string;
  amount: number;
  fee: number;
  total?: number;
}
