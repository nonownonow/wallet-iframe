export interface TransactionQuery {
  send: string;
  [key: string]: string;
}
export type FaceWalletAction =
  | { type: 'CREATE_WALLET' }
  | {
      type: 'TRANSACTION';
      payload: TransactionQuery;
    };
export interface FaceWalletMessage {
  id?: string;
  serviceId?: string;
  action?: FaceWalletAction;
  result?: string;
  error?: string;
}
